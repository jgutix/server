# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [0.1.0](https://gitlab.coko.foundation/cokoapps/server/compare/v0.0.2...v0.1.0) (2020-04-04)


### Features

* **server:** cors config to allow client running on a different port ([dff70dd](https://gitlab.coko.foundation/cokoapps/server/commit/dff70dd2623adc3855129f1e98ba1cda68f37a0d))

### [0.0.2](https://gitlab.coko.foundation/cokoapps/server/compare/v0.0.1...v0.0.2) (2020-03-28)


### Features

* bundle pubsweet cli with package ([0c4b206](https://gitlab.coko.foundation/cokoapps/server/commit/0c4b2060a6f453a12408bdb786967b6d709c2220))
* **server:** add cron support ([dcd352a](https://gitlab.coko.foundation/cokoapps/server/commit/dcd352ade1cc96583a1d86366fcc0a21aee77961))


### Bug Fixes

* **server:** resolve circular dependencies & make passport auth work ([5dffd0f](https://gitlab.coko.foundation/cokoapps/server/commit/5dffd0fbd65753181b4ac171cac40dd1b10df234))

### 0.0.1 (2020-03-27)


### Features

* **server:** export express app ([bdbab7d](https://gitlab.coko.foundation/cokoapps/server/commit/bdbab7d71d1ba8518ab40f60a8869975adf5dff8))
